
package examencontornosproductos;

import java.util.TreeMap;


public class ExamenContornosProductos {   
    public static void main(String[] args) {
        //Creación del Treemap y las clases correspondientes con sus nombres,precios y claves.
    TreeMap <String, Producto> productos = new TreeMap();
    Producto producto1 = new Producto("Cafetera", 10);
     productos.put("Clave1",producto1);
    Producto producto2 = new Producto("Tetera", 11);
     productos.put("Clave2",producto2);
    Producto producto3 = new Producto("Tostadora", 12);
     productos.put("Clave3",producto3);
    Producto producto4 = new Producto("Batidora", 13);
     productos.put("Clave4",producto4);
    Producto producto5 = new Producto("Servilletera", 14);
     productos.put("Clave5",producto5);
     
     //Visualización de los productos y sus datos
      for(int i=0;i<5;i++){
          System.out.println(productos.get("Clave"+(i+1)).nombre+" Precio: "+productos.get("Clave"+(i+1)).precio);
      } 
      for(int i=0;i<5;i++){
productos.remove("Clave"+(i+1));
     }
}
}
